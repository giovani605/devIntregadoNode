"use strict"

const express = require('express');
const router = express.Router();
var banco = require("../../../model/bancoDados");

// nesse arquivo fica a rota que lida com os cartoes
//recuperar o dado do cartao
router.get("/:id", (req, res, next) => {
    recuperarSaldoAtual(req.params.id, (dados) => {
        res.status(200).send({
            "mensagem": "ok",
            "dados": dados
        })

    });
});

// Atualiza o saldo do cartao
router.post("/atualizar/:id", (req, res, next) => {
    console.log("Atualizando Saldo cartao " + req.params.id);
    recuperarSaldoAtual(req.params.id, (dados) => {
        res.status(200).send({
            "mensagem": "ok",
            "dados": dados
        })

    });
});


router.get("/user/:id", (req, res, next) => {

    var resposta = {};
    banco.buscarCartoesUsuario(req.params.id, (dados) => {
        res.status(200).send({
            "mensagem": "ok",
            "dados": dados
        });

    });
});


// Rota para calcular o valor em um inicio e fim de periodo
router.post("/periodo/:id", (req, res, next) => {
    var dataInicial = new Date(req.body.dataInicial);
    var dataFinal = new Date(req.body.dataFinal);

    // bug estranho q so ocorre aqui
    dataFinal.setDate(dataFinal.getDate() +1);
    dataInicial.setDate(dataInicial.getDate() +1);


    console.log("Data Inicial " + dataInicial + " data final " + dataFinal);

    var idCartao = req.params.id
    console.log("id" + JSON.stringify(req.body));

    calcularSaldoData(idCartao, dataInicial, (dadosInicio) => {
        calcularSaldoData(idCartao, dataFinal, (dadosFinal) => {
            // fazer a resposta
            res.status(200).send({
                "mensagem": "ok",
                "inicio": dadosInicio,
                "fim": dadosFinal
            });
        });

    });


});

router.post("/cache/:id", (req, res, next) => {
    var dataFinal = new Date(req.body.dataFinal);
    var idCartao = req.params.id;
    console.log("id" + idCartao);
    criarCacheSaldo(idCartao, dataFinal, (dados) => {
        res.status(200).send({
            "mensagem": "ok",
            "dados": dados
        });
    });
});

// Recupera ultimo cache de Saldo
function recuperarCacheSaldoRecente(idCartao, callback) {

    banco.buscarUltimoCacheSaldo(idCartao, (resultado) => {
        callback(resultado);
    });

}
// Recupera o Cache de saldo mais perto da data
function recuperarCacheSaldoMaisPertoData(idCartao, data, callback) {
    banco.buscarUltimoCacheSaldoPerto(idCartao, data, (resultado) => {
        callback(resultado);
    });

}
// Recupera Saldo Atual do cartao
// Caso necessario tbm calcula oq for necessario para atualizar o saldo
function recuperarSaldoAtual(idCartao, callback) {
    recuperarCacheSaldoRecente(idCartao, (resultado) => {
        var saldoSalvo;
        var inicio;
        if (resultado == null) {
            var saldoSalvo = 0;
            inicio = new Date("2010-1-1");
        } else {
            var saldoSalvo = resultado["saldo"];
            inicio = new Date(resultado["periodo_final"]);
        }


        // soma um dia para frente
        inicio.setDate(inicio.getDate() + 1);
        console.log("Data de inicio " + inicio);
        banco.buscarTransacoesPeriodo(idCartao, inicio, new Date(), (transacoes) => {
            var total = somarSaldo(transacoes);
            console.log(saldoSalvo);
            var saldoAtual = Number(saldoSalvo) + Number(total);
            console.log(saldoAtual);
            // posso ate atualizar isso no database
            banco.atualizarSaldoCartao(idCartao, saldoAtual);

            var final = {
                "saldo": saldoAtual,
                "trasacoes": transacoes,
                "cache": resultado
            }
            callback(final);
        });
    });
}

// Insere no banco um "cache" de saldo sobre o periodo
function criarCacheSaldo(idCartao, dataFim, callback) {

    // consultar transações por periodo
    recuperarCacheSaldoRecente(idCartao, (resultado) => {
        var saldoSalvo;
        var inicio;
        if (!resultado) {
            var saldoSalvo = 0;
            var inicio = new Date('2012-01-01');
        } else {
            var saldoSalvo = resultado["saldo"];
            var inicio = new Date(resultado["periodo_final"]);
        }
        // com base nisso .... 
        console.log("Data de inicio " + inicio);
        // soma um dia para frente
        inicio.setDate(inicio.getDate() + 1);
        banco.buscarTransacoesPeriodo(idCartao, inicio, dataFim, (transacoes) => {
            var total = somarSaldo(transacoes);
            console.log(saldoSalvo);
            var saldoAtual = Number(saldoSalvo) + Number(total);
            console.log(saldoAtual);
            // posso ate atualizar isso no database
            banco.atualizarSaldoCartao(idCartao, saldoAtual);
            banco.inserirCacheSaldo(idCartao, saldoAtual, inicio, dataFim, (resposta) => {
                callback({
                    "sucesso": resposta,
                    "saldo": saldoAtual,
                });
            });

        });
    });

}

// recebe uma lista de transacoes e retorna o valor delas
function somarSaldo(ListaTransacoes) {
    var total = 0;
    for (let t of ListaTransacoes) {
        total += Number(t["valor"]);
    }
    return total;
}


// Calcula o saldo do carato em um Data
function calcularSaldoData(idCartao, data, callback) {
    recuperarCacheSaldoMaisPertoData(idCartao, data, (cache) => {

        var saldoSalvo;
        var inicio;
        if (cache == null) {
            var saldoSalvo = 0;
            inicio = new Date("2010-1-1");
        } else {
            var saldoSalvo = cache["saldo"];
            inicio = new Date(cache["periodo_final"]);
        }


        // soma um dia para frente
        inicio.setDate(inicio.getDate() + 1);
        console.log("Data de inicio " + inicio);


        banco.buscarTransacoesPeriodo(idCartao, inicio, data, (transacoes) => {
            // calculo e retorno
            var total = somarSaldo(transacoes);
            console.log(saldoSalvo);
            var saldoAtual = Number(saldoSalvo) + Number(total);
            console.log(saldoAtual);
            // posso ate atualizar isso no database
            var final = {
                "saldo": saldoAtual,
                "trasacoes": transacoes,
                "cache": cache
            }
            callback(final);


        });

    });
}

module.exports = router;